package volodyko.com.quadcompressor.hist;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import volodyko.com.quadcompressor.LoadImagesTask;
import volodyko.com.quadcompressor.phash.ReadImage;

/**
 * Created by volodyko on 27.01.17.
 */

public class HistogramAPI {
    private static final String TAG = "HistogramAPI";

    private static boolean isSimilar(int[] targetHist, int[] candidateHist) throws IncompatibleHistogramsException {
        boolean isSimilar = false;
        float similarity = 0;
        float threshold = 1;
        if (targetHist.length != candidateHist.length) {
            throw new IncompatibleHistogramsException();
        }

        for (int i = 0; i < targetHist.length; i++) {
            similarity += Math.sqrt(targetHist[i] * candidateHist[i]);
        }
        Log.d(TAG, "isSimilar: " + similarity);
        if (similarity > threshold) {
            isSimilar = true;
        }

        return isSimilar;
    }

    private static List<File> getAllImagesFromDirectory(String directoryPath) {
        List<File> imagesPathList = new ArrayList<>();
        LoadImagesTask task = new LoadImagesTask();
        long start = System.currentTimeMillis();
        Log.d(TAG, "useImage: " + start);

        List<File> fileList = task.getListOfFiles(LoadImagesTask.FileType.IMAGE);
        for (File file : fileList) {
            imagesPathList.add(file);
            Log.d(TAG, "getAllImagesFromDirectory: " + file.getName());
        }

        long end = System.currentTimeMillis();
        Log.d(TAG, "useImage: " + (end - start));
        return imagesPathList;
    }

    private static int[] getIamgeHistogram(Bitmap bi) {
        int width = bi.getWidth();
        int height = bi.getHeight();
        int anzpixel = width * height;
        int[] histogram = new int[255];
        int i = 0;


        //read pixel intensities into histogram
        for (int x = 1; x < width; x++) {
            for (int y = 1; y < height; y++) {
                int pixel = bi.getPixel(x, y);
                int color = Color.rgb(pixel)
                int valueBefore = Color.red(pixel);
                histogram[valueBefore]++;

                & 0xFF
            }
        }

        int sum = 0;
        // build a Lookup table LUT containing scale factor
        float[] lut = new float[anzpixel];
        for (i = 0; i < 255; ++i) {
            sum += histogram[i];
            lut[i] = sum * 255 / anzpixel;
        }

        // transform image using sum histogram as a Lookup table
      /*  for (int x = 1; x < width; x++) {
            for (int y = 1; y < height; y++) {
                int valueBefore = bi.getPixel(x, y) & 0xff;
                int valueAfter = (int) lut[valueBefore];
                bi.setPixel(x, y, valueAfter);
            }
        }*/
        Log.d(TAG, "getIamgeHistogram: " + Arrays.toString(histogram));

        return histogram;
    }


    public static Bitmap toGrayscale(Bitmap bmpOriginal, int width, int height) {

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public static int[][] getGreyPixelArray(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int[][] greyPixelArray = new int[height][width];
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                greyPixelArray[y][x] = image.getPixel(x, y) & 0xFF;
        return greyPixelArray;
    }

    @Nullable
    private static Bitmap getBitmap(File filePath, Map<String, Integer> imageAtrributes) {
        Bitmap loadedImage = null;
        try {
            loadedImage = ReadImage.readFromPath(filePath, imageAtrributes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (loadedImage == null || imageAtrributes.get(ReadImage.IMAGE_HEIGHT) == null
                || imageAtrributes.get(ReadImage.IMAGE_WIDTH) == null)
            return null;
        return loadedImage;
    }

    public static class IncompatibleHistogramsException extends Throwable {
        @Override
        public String getMessage() {
            return "Incompatible Histograms";
        }
    }

    public static void findDublicates(String directoryPath) throws IncompatibleHistogramsException {
        List<File> imagesFileList = getAllImagesFromDirectory(directoryPath);
        if (null == imagesFileList || imagesFileList.isEmpty())
            return;


        Map<String, Integer> imageAttributes = new HashMap<>();
        Map<String, int[]> imagesHistMap = new LinkedHashMap<>();
        Map<String, Integer> imagesMap = new HashMap<>();
        int[] imageHist;

        for (File imagesFile : imagesFileList) {
            imageAttributes.clear();
            imageHist = null;
            imageHist = getIamgeHistogram(getBitmap(imagesFile, imageAttributes));
            if (imageHist != null && imageHist.length > 0) {
                imagesMap.put(imagesFile.getAbsolutePath(), imageAttributes.get(ReadImage.IMAGE_WIDTH) * imageAttributes.get(ReadImage.IMAGE_WIDTH));
                imagesHistMap.put(imagesFile.getAbsolutePath(), imageHist);
            }
        }

        int totalImagesCount = imagesMap.size();
        Log.d(TAG, "findDuplicateImagePairs: " + totalImagesCount);

        for (Map.Entry<String, int[]> entry1 : imagesHistMap.entrySet()) {
            for (Map.Entry<String, int[]> entry2 : imagesHistMap.entrySet()) {
                if (!entry1.getKey().equalsIgnoreCase(entry2.getKey()) && isSimilar(entry1.getValue(), entry2.getValue())) {
                    Log.d(TAG, "findDublicates: " + entry1.getKey() + " & " + entry2.getKey() + " are similar");
                }
            }
        }
    }
}
