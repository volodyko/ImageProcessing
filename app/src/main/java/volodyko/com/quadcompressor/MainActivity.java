package volodyko.com.quadcompressor;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;

import volodyko.com.quadcompressor.hist.HistogramAPI;

public class MainActivity extends AppCompatActivity {

    private static final int SELECT_PHOTO = 10;
    private ListView listView;
    private ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list);
        adapter = new ListAdapter(this, new ArrayList<String>());
        listView.setAdapter(adapter);
        try {
            HistogramAPI.findDublicates("");
        } catch (HistogramAPI.IncompatibleHistogramsException e) {
            e.printStackTrace();
        }
       /* findViewById(R.id.open).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> items = new ArrayList<>();
                long start = System.currentTimeMillis();
                List<List<String>> res = API.findDuplicatePairs("");
                long finish = System.currentTimeMillis() - start;
                Log.d(TAG, "onClick: " + finish);
                //Tree = 13372
                //Balanced 13464
                //HashMaps = 13356

                if (res != null) {
                    for (List<String> list : res) {
                        Log.d(TAG, "onClick: row");
                        for (String s : list) {
                            items.add(s);
                            Log.d(TAG, "onClick: " + s);
                        }
                    }
                }
                adapter.setItemname(items);

                adapter.notifyDataSetChanged();
            }
        });*/

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 213);
    }

    private void getPhoto() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == SELECT_PHOTO) {
            if (resultCode == RESULT_OK) {
                if (intent != null) {
                    // Get the URI of the selected file
                    final Uri uri = intent.getData();
                    try {
                        useImage(uri);
                    } catch (IOException e) {

                    }
                }
            }
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    private static final String TAG = "MainActivity";

    void useImage(Uri uri) throws IOException {
        /*long start = System.currentTimeMillis();
        Log.d(TAG, "useImage: " + start);
        quadTree = new Quad(accuracy, uri, this);
        long end = System.currentTimeMillis();
        Log.d(TAG, "useImage: " + (end - start));
        Bitmap bitmap1 = Bitmap.createBitmap(quadTree.getWidth(), quadTree.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap1);
        Paint p = new Paint();
        p.setStyle(Paint.Style.STROKE);
        int level = quadTree.height() - 1;
        paintImage(canvas, p, quadTree.getRoot(), level);
        //use the bitmap as you like
        imageView.setImageBitmap(bitmap1);*/
    }


}
