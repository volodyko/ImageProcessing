package volodyko.com.quadcompressor.phash;

import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import volodyko.com.quadcompressor.LoadImagesTask;
import volodyko.com.quadcompressor.tree.BalancedHashTree;
import volodyko.com.quadcompressor.tree.Comparable;
import volodyko.com.quadcompressor.tree.ImageData;
import volodyko.com.quadcompressor.tree.Traversal;

/**
 * Created by volodyko on 20.12.16.
 */

public class API {
    private static final String TAG = "API";

    private static ImagePHash pHash = new ImagePHash(32, 8);


    private static boolean isSimilar(String hashA, String hashB) {
        if (hashA.length() != hashB.length()) {
            return false;
        }
        int distance = 0;
        Log.d(TAG, "isSimilar: " + hashA + " " + hashB);
        for (int x = 0; x < hashA.length(); x++) {
            if (hashA.charAt(x) != hashB.charAt(x))
                distance++;
        }
        Log.d(TAG, "isSimilar: " + distance);
        if (distance <= 10)
            return true;
        else
            return false;
    }

    private static List<File> getAllImagesFromDirectory(String directoryPath) {
        List<File> imagesPathList = new ArrayList<>();
        LoadImagesTask task = new LoadImagesTask();
        long start = System.currentTimeMillis();
        Log.d(TAG, "useImage: " + start);

        List<File> fileList = task.getListOfFiles(LoadImagesTask.FileType.IMAGE);
        for (File file : fileList) {
            imagesPathList.add(file);
            Log.d(TAG, "getAllImagesFromDirectory: " + file.getName());
        }

        long end = System.currentTimeMillis();
        Log.d(TAG, "useImage: " + (end - start));
        return imagesPathList;
    }

    public static List<List<String>> findDuplicatePairs(String directoryPath) {
        List<Map<String, Integer>> pairs = findDuplicateImagePairs(directoryPath);
        List<List<String>> output = new ArrayList<List<String>>();
        if (null == pairs || pairs.isEmpty())
            return null;
        for (Map<String, Integer> pair : pairs) {
            if (!pair.isEmpty() && pair.keySet().size() > 1) {
                List<String> outputElement = new ArrayList<String>(pair.keySet());
                output.add(outputElement);
            }
        }
        return output;
    }

    public static List<List<String>> findDuplicatePairsTree(String directoryPath) {
        List<String> pairs = findDuplicateImagePairsTree(directoryPath);
        List<List<String>> output = new ArrayList<List<String>>();
        if (null == pairs || pairs.isEmpty())
            return null;
        for (String pair : pairs) {
            if (!pair.isEmpty()) {
                List<String> outputElement = new ArrayList<String>(pairs);
                output.add(outputElement);
            }
        }
        return output;
    }

    private static BalancedHashTree tree;

    private static List<String> findDuplicateImagePairsTree(String directoryPath) {
        final List<String> output = new ArrayList<>();

        tree = new BalancedHashTree(new Comparable() {
            @Override
            public int compare(ImageData a, ImageData b) {
                return a.getHash().compareTo(b.getHash());
            }
        });
        String imageHash = null;
        List<File> imagesFileList = getAllImagesFromDirectory(directoryPath);
        if (null == imagesFileList || imagesFileList.isEmpty())
            return null;

        Map<String, Integer> imageAttributes = new HashMap<>();

        for (File imagesFile : imagesFileList) {
            imageAttributes.clear();
            imageHash = pHash.culcPHash(ImageProcessing.getBitmap(imagesFile, imageAttributes));
            tree.add(new ImageData(imageHash, imageAttributes.get(ReadImage.IMAGE_WIDTH) * imageAttributes.get(ReadImage.IMAGE_HEIGHT), imagesFile.getAbsolutePath()));
        }
        int totalImagesCount = tree.size();
        Log.d(TAG, "findDuplicateImagePairs: " + totalImagesCount);
        tree.traverse(new Traversal() {
            @Override
            public void process(final ImageData entry1) {
                tree.traverse(new Traversal() {
                    @Override
                    public void process(ImageData entry2) {
                        String hash1 = entry1.getHash();
                        String hash2 = entry2.getHash();
                        boolean distance = pHash.distance(hash1, hash2);
                        if (!hash1.equalsIgnoreCase(hash2) && distance) {
                            Log.d(TAG, "process: similar" + hash1 + " " + hash2);
                            output.add(entry1.getPath());
                            output.add(entry2.getPath());
                        }
                    }
                });
            }
        });

        return output;
    }

    private static List<Map<String, Integer>> findDuplicateImagePairs(String directoryPath) {
        List<File> imagesFileList = getAllImagesFromDirectory(directoryPath);
        if (null == imagesFileList || imagesFileList.isEmpty())
            return null;
        Map<String, Integer> imagesMap = new HashMap<>();
        Map<String, String> imagesHashMap = new LinkedHashMap<>();
        Map<String, Integer> imageAttributes = new HashMap<>();
        List<Map<String, Integer>> output = new ArrayList<>();
        Set<String> allImagesFoundDuplicates = new HashSet<>();
        boolean isImageFoundInPair = false;
        String imageHash = null;
        for (File imagesFile : imagesFileList) {
            imageAttributes.clear();
            imageHash = null;
            //imageHash = ImageProcessing.getImageHash(imagesFile, imageAttributes);
            imageHash = pHash.culcPHash(ImageProcessing.getBitmap(imagesFile, imageAttributes));
            if (imageHash != null && !imageHash.isEmpty()) {
                imagesMap.put(imagesFile.getAbsolutePath(), imageAttributes.get(ReadImage.IMAGE_WIDTH) * imageAttributes.get(ReadImage.IMAGE_WIDTH));
                imagesHashMap.put(imagesFile.getAbsolutePath(), imageHash);
            }
        }
        int totalImagesCount = imagesMap.size();
        Log.d(TAG, "findDuplicateImagePairs: " + totalImagesCount);
        for (Map.Entry<String, String> entry1 : imagesHashMap.entrySet())
            for (Map.Entry<String, String> entry2 : imagesHashMap.entrySet()) {
                if (!entry1.getKey().equalsIgnoreCase(entry2.getKey()) && pHash.distance(entry1.getValue(), entry2.getValue())) {
                       /* isSimilar(entry1.getValue(), entry2.getValue()*/
                    isImageFoundInPair = false;
                    for (Map outputMap : output) {
                        if (outputMap.containsKey(entry1.getKey()) || outputMap.containsKey(entry2.getKey())) {
                            outputMap.put(entry1.getKey(), imagesMap.get(entry1.getKey()));
                            outputMap.put(entry2.getKey(), imagesMap.get(entry2.getKey()));
                            isImageFoundInPair = true;
                            break;
                        }
                    }
                    if (!isImageFoundInPair) {
                        Map<String, Integer> outputMap = new HashMap<>();
                        outputMap.put(entry1.getKey(), imagesMap.get(entry1.getKey()));
                        outputMap.put(entry2.getKey(), imagesMap.get(entry2.getKey()));
                        output.add(outputMap);
                    }
                    allImagesFoundDuplicates.add(entry1.getKey());
                    allImagesFoundDuplicates.add(entry2.getKey());
                }
            }

        Set<String> allImages = new HashSet<>(imagesMap.keySet());
        allImages.removeAll(allImagesFoundDuplicates);
        for (String outputElement : allImages) {
            Map<String, Integer> outputMap = new HashMap<>();
            outputMap.put(outputElement, imagesMap.get(outputElement));
            output.add(outputMap);
        }
        return output;
    }
}
