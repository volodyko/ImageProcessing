package volodyko.com.quadcompressor.phash;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by volodyko on 20.12.16.
 */

public class ReadImage {
    public static final String IMAGE_WIDTH = "IMAGE_WIDTH";
    public static final String IMAGE_HEIGHT = "IMAGE_HEIGHT";

    private ReadImage() {
    }

    public static Bitmap readFromPath(String filePath, Map<String, Integer> imageAtrributes) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        imageAtrributes.put(IMAGE_WIDTH, bitmap.getWidth());
        imageAtrributes.put(IMAGE_HEIGHT, bitmap.getHeight());
        return bitmap;
    }

    private static final String TAG = "ReadImage";

    public static Bitmap readFromPath(File filePath, Map<String, Integer> imageAtrributes) throws IOException {
        Bitmap bitmap = BitmapFactory.decodeFile(filePath.getAbsolutePath());
        if (bitmap != null) {
            imageAtrributes.put(IMAGE_WIDTH, bitmap.getWidth());
            imageAtrributes.put(IMAGE_HEIGHT, bitmap.getHeight());
        }
        return bitmap;
    }

}
