package volodyko.com.quadcompressor.phash;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.support.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by volodyko on 20.12.16.
 */

public class ImageProcessing {


    public static Bitmap toGrayscale(Bitmap bmpOriginal, int width, int height) {

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public static int[][] getGreyPixelArray(Bitmap image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int[][] greyPixelArray = new int[height][width];
        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
                greyPixelArray[y][x] = image.getPixel(x, y) & 0xFF;
        return greyPixelArray;
    }


    public static String generateImageHash(int[][] colorArray) {
        StringBuffer sb = new StringBuffer();

        int height = colorArray.length;
        int width = colorArray[0].length - 1;
        for (int y = 0; y < width; y++) {
            for (int x = 0; x < height; x++) {
                sb.append(colorArray[y][x] < colorArray[y][x + 1] ? "1" : "0");
            }
        }
        return sb.toString();
    }

    public static long hashBitmap(Bitmap bmp) {
        long hash = 31; //or a higher prime at your choice
        for (int x = 0; x < bmp.getWidth(); x++) {
            for (int y = 0; y < bmp.getHeight(); y++) {
                hash *= (bmp.getPixel(x, y) + 31);
            }
        }
        return hash;
    }

    public static String getImageHash(File filePath, Map<String, Integer> imageAtrributes) {
        String returnValue = null;

        try {
            Bitmap loadedImage = getBitmap(filePath, imageAtrributes);
            if (loadedImage == null) return null;
            Bitmap gray = toGrayscale(loadedImage, loadedImage.getWidth() / 100, loadedImage.getHeight() / 100 - 1);
            int[][] color = getGreyPixelArray(gray);
            returnValue = generateImageHash(color);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    @Nullable
    public static Bitmap getBitmap(File filePath, Map<String, Integer> imageAtrributes) {
        Bitmap loadedImage = null;
        try {
            loadedImage = ReadImage.readFromPath(filePath, imageAtrributes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (loadedImage == null || imageAtrributes.get(ReadImage.IMAGE_HEIGHT) == null
                || imageAtrributes.get(ReadImage.IMAGE_WIDTH) == null)
            return null;
        return loadedImage;
    }
}
