package volodyko.com.quadcompressor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by volodyko on 20.12.16.
 */

public class ListAdapter extends BaseAdapter {
    private Context context;
    private List<String> itemname;

    public ListAdapter(@NonNull Context context, List<String> itemname) {
        this.context = context;
        this.itemname = itemname;
    }

    public void setItemname(List<String> itemname) {
        this.itemname = itemname;
    }

    @Override
    public int getCount() {
        return itemname.size();
    }

    @Override
    public Object getItem(int i) {
        return itemname.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.mylist, null, true);
            ImageView imageView = (ImageView) view.findViewById(R.id.icon);
            holder = new ViewHolder();
            holder.imageView = imageView;

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Bitmap bitmap = BitmapFactory.decodeFile(itemname.get(position));
        holder.imageView.setImageBitmap(bitmap);

        return view;

    }

    static class ViewHolder {
        ImageView imageView;
    }
}
