package volodyko.com.quadcompressor.quad;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.ColorInt;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by volodyko on 19.12.16.
 */

public class Quad {
    private Node root;
    private double accuracy;
    private int width;
    private int height;

    public Quad(double accuracy, Bitmap bitmap) {

        this.accuracy = accuracy;

        int[][] image = bitmapToPixels(bitmap);

        root = compress(image, 0, 0, width, height);
    }

    public Quad(double accuracy, Uri photoPath, Context context) throws IOException {
        this.accuracy = accuracy;

        Bitmap bitmap = getBitmap(context, photoPath);
        int[][] image = bitmapToPixels(bitmap);

        root = compress(image, 0, 0, width, height);
    }

    private Node compress(int[][] image, int i, int j, int h, int w) {
        Node node = new Node(i, j, h, w);
        @ColorInt
        int color;

        if (h == 1 && w == 1) {
            node.setColor(image[i][j]);
        } else if (h == 1 || w == 1) {

            if (h == 1) {

                if (w <= 4) {

                    for (int k = 0; k < w; ++k) {
                        node.getChild()[k] = compress(image, i, j + k, h, 1);
                    }

                } else {

                    int w_ = w / 2;

                    node.getChild()[0] = compress(image, i, j, 1, w_);
                    node.getChild()[1] = compress(image, i, j + w_, 1, w - w_);

                }

            } else {

                if (h <= 4) {

                    for (int k = 0; k < h; ++k) {
                        node.getChild()[k] = compress(image, i + k, j, 1, w);
                    }

                } else {

                    int h_ = h / 2;

                    node.getChild()[0] = compress(image, i, j, h_, 1);
                    node.getChild()[1] = compress(image, i + h_, j, h - h_, 1);

                }

            }

            node.setColor(node.mediumChildren());


        } else if ((color = getNodeColor(image, i, j, h, w)) != -1) {
            node.setColor(color);
        } else {

            int h_ = h / 2;
            int w_ = w / 2;

            node.getChild()[0] = compress(image, i, j + w_, h_, w - w_);
            node.getChild()[1] = compress(image, i, j, h_, w_);
            node.getChild()[2] = compress(image, i + h_, j, h - h_, w_);
            node.getChild()[3] = compress(image, i + h_, j + w_, h - h_, w - w_);

            node.setColor(node.mediumChildren());

        }

        return node;
    }

    @ColorInt
    private int getNodeColor(int[][] image, int i, int j, int height, int width) {
        Map<Integer, Integer> colors = new HashMap<>();

        int r = 0;
        int g = 0;
        int b = 0;
        int a = 0;

        for (int k = 0; k < height; ++k) {

            for (int l = 0; l < width; ++l) {
                @ColorInt
                int c = image[k + i][l + j];

                r = r + Color.red(c);
                g = g + Color.green(c);
                b = b + Color.blue(c);
                a = a + Color.alpha(c);

                if (!colors.containsKey(c)) {
                    colors.put(c, 1);
                } else {
                    int count = colors.get(c) + 1;
                    colors.put(c, count);
                }

            }

        }

        int size = height * width;

        r = r / size;
        g = g / size;
        b = b / size;
        a = a / size;

        int max = 0;
        for (int c : colors.keySet()) {

            int count = colors.get(c);
            max = Math.max(max, count);

        }

        double currentAccuracy = (double) max / size;

        return (currentAccuracy >= accuracy) ? Color.argb(a, r, g, b) : -1;
    }

    public int height() {
        return height(root);
    }

    private int height(Node root) {
        if (root == null) return 0;
        int h1 = height(root.getChild()[0]);
        int h2 = height(root.getChild()[1]);
        int h3 = height(root.getChild()[2]);
        int h4 = height(root.getChild()[3]);

        int max = (h1 > h2) ? h1 : (h2 > h3) ? h2 : (h3 > h4) ? h3 : h4;
        return max + 1;
    }

    private int[][] bitmapToPixels(Bitmap bitmap) {
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        int[][] result = new int[width][height];
        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        int pixelsIndex = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                result[i][j] = pixels[pixelsIndex];
                pixelsIndex++;
            }
        }
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
        }
        return result;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio){
        int k = Integer.highestOneBit((int)Math.floor(ratio));
        if(k==0) return 1;
        else return k;
    }

    private Bitmap getBitmap(Context context, Uri photoPath) throws IOException {
        InputStream input = context.getContentResolver().openInputStream(photoPath);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_4444;
        options.inSampleSize = getPowerOfTwoForSampleRatio(16);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, options);
        input.close();
        return bitmap;
    }


    public Node getRoot() {
        return root;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void paintImage(Canvas g2d, Paint p, Node node, int level) {
        if (level == 0 || node.isLeaf()) {

            int c = node.getColor();

            int x = node.getX();
            int y = node.getY();

            int w = node.getWidth();
            int h = node.getHeight();

            p.setColor(c);

            g2d.drawRect(x, y, w, h, p);
            return;

        }

        for (int i = 0; i < node.getChild().length; ++i) {

            if (node.getChild()[i] != null) {
                paintImage(g2d, p, node.getChild()[i], level - 1);
            }

        }

    }
}
