package volodyko.com.quadcompressor.quad;


import android.graphics.Color;
import android.support.annotation.ColorInt;

/**
 * Created by volodyko on 19.12.16.
 */

public class Node {
    private int x;
    private int y;
    private int width;
    private int height;

    @ColorInt
    private int color;

    private Node child[];

    public Node(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        child = new Node[4];
    }

    public boolean isLeaf() {
        return child != null && child[0] == null && child[1] == null && child[2] == null && child[3] == null;
    }

    @ColorInt
    public int mediumChildren() {
        int r = 0;
        int g = 0;
        int b = 0;
        int a = 0;

        int div = 0;
        for (int i = 0; i < child.length; i++) {
            if (child[i] != null) {
                r = r + Color.red(child[i].getColor());
                g = g + Color.green(child[i].getColor());
                b = b + Color.blue(child[i].getColor());
                a = a + Color.alpha(child[i].getColor());

                div++;
            }
        }
        r = r / div;
        g = g / div;
        b = b / div;
        a = a / div;

        return Color.argb(a, r, g, b);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @ColorInt
    public int getColor() {
        return color;
    }

    public void setColor(@ColorInt int color) {
        this.color = color;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Node[] getChild() {
        return child;
    }
}
