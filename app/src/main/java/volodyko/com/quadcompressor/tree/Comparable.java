package volodyko.com.quadcompressor.tree;

/**
 * Created by volodyko on 05.01.17.
 */

public interface Comparable {
    int compare(ImageData a, ImageData b);
}
