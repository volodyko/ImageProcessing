package volodyko.com.quadcompressor.tree;

/**
 * Created by volodyko on 05.01.17.
 */

public class ImageData {
    private String hash;
    private int attr;
    private String path;

    public ImageData(String hash, int attr, String path) {
        this.hash = hash;
        this.attr = attr;
        this.path = path;
    }

    public String getHash() {
        return hash;
    }

    public int getAttr() {
        return attr;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return "ImageData{" +
                "hash='" + hash + '\'' +
                ", attr=" + attr +
                '}';
    }
}
