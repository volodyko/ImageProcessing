package volodyko.com.quadcompressor.tree;

/**
 * Created by volodyko on 05.01.17.
 */

public class BalancedHashTree extends HashTree {

    protected int count = 0;


    public BalancedHashTree(Comparable comparable) {
        super(comparable);
    }

    public void add(ImageData data) {
        super.add(data);
        if (root != null) {
            root = balance(root);
        }
    }

    protected int branchCount(TreeNode root, int
            direction) {
        count = 0;
        TreeNode branch = null;
        if (root == null)
            return 0;
        switch (direction) {
            case RIGHT:
                branch = root.getRight();
                break;
            case LEFT:
                branch = root.getLeft();
                break;
        }
        if (branch == null)
            return 0;

        traverse(branch, INORDER, new Traversal() {
                    public void process(ImageData o) {
                        count++;
                    }
                }
        );
        return count;
    }

    protected TreeNode rotate(TreeNode root, int
            direction) {
        TreeNode newRoot = null;
        TreeNode orphan = null;
        switch (direction) {
            case RIGHT:
                newRoot = root.getLeft();
                root.setLeft(null);
                orphan = newRoot.getRight();
                newRoot.setRight(root);
                break;
            case LEFT:
                newRoot = root.getRight();
                root.setRight(null);
                orphan = newRoot.getLeft();
                newRoot.setLeft(root);
                break;
        }
        if (newRoot == null)
            return root;
        if (orphan != null)
            add(root, orphan);
        return newRoot;
    }

    protected TreeNode balance(TreeNode root) {
        if (root == null)
            return null;
        int left = branchCount(root, LEFT);
        int right = branchCount(root, RIGHT);
        if (left > right) {
            while (left > right + 1) {
                root = rotate(root, RIGHT);
                left = branchCount(root, LEFT);
                right = branchCount(root, RIGHT);
            }
        }
        if (right > left) {
            while (right > left + 1) {
                root = rotate(root, LEFT);
                left = branchCount(root, LEFT);
                right = branchCount(root, RIGHT);
            }
        }
        root.setLeft(balance(root.getLeft()));
        root.setRight(balance(root.getRight()));
        return root;
    }
}
