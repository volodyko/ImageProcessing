package volodyko.com.quadcompressor;

import android.os.Environment;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by volodyko on 20.12.16.
 */

public class LoadImagesTask {
    private static final int TEN_MB = 1 * 1024 * 1024;

    public List<File> getListOfFiles(FileType fileType) {
        File dir = Environment.getExternalStorageDirectory();
        List<File> files = getFiles(dir, fileType);
        return files;
    }

    private List<File> getFiles(File dir, FileType fileType) {
        Stack<File> stack = new Stack<File>();
        List<File> filesList = new ArrayList<>();
        stack.push(dir);
        while (!stack.isEmpty()) {
            File child = stack.pop();
            if (child != null && child.isDirectory()) {
                File[] listFiles = child.listFiles();
                if (null != listFiles) {
                    for (int i = 0; i < listFiles.length; i++) {
                        File f = listFiles[i];
                        if (f != null) {
                            stack.push(f);
                        }
                    }
                }
            } else if (child != null && child.isFile()) {
                if (child.length() >= TEN_MB && fileType == getFileType(child.getName().toLowerCase())) {
                    filesList.add(child);
                }
            }
        }
        return filesList;
    }

    public enum FileType {
        VIDEO, IMAGE, MUSIC, OTHERS, ALL
    }

    public static String getExtension(String name) {
        String ext;
        if (name.lastIndexOf(".") == -1) {
            ext = "";

        } else {
            int index = name.lastIndexOf(".");
            ext = name.substring(index + 1, name.length());
        }
        return ext;
    }

    public static String getMimeFromFileName(String filename) {
        String type = null;
        String extension = getExtension(filename);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    private FileType getFileType(String fileName) {
        FileType res = FileType.OTHERS;
        String mimeType = getMimeFromFileName(fileName);
        if (!TextUtils.isEmpty(mimeType)) {
            if (mimeType.startsWith("video")) {
                res = FileType.VIDEO;
            }

            if (mimeType.startsWith("image")) {
                res = FileType.IMAGE;
            }

            if (mimeType.startsWith("audio")) {
                res = FileType.MUSIC;
            }
        }
        return res;
    }
}
